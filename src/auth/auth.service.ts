import { HttpCode, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  async validateUser(
    username: string,
    pass: string,
  ): Promise<{ username: string; password: string } | null> {
    console.log(username,pass)
    if (
      username === (process.env.ADMIN_LOGIN ?? 'toor') &&
      (process.env.ADMIN_PASSWORD ?? 'admin') === pass
    ) {
      return { username: username, password: pass };
    }
    return null;
  }

  async login(user: { username: string; password: string }) {
    if (user.username===(process.env.ADMIN_LOGIN ?? 'toor') && user.password===(process.env.ADMIN_PASSWORD ?? 'admin')){
      const payload = { username: user.username, sub: user.password };
      return {
        access_token: this.jwtService.sign(payload),
      };
    } else {
      throw new HttpException('Wrong creds are passed',HttpStatus.BAD_REQUEST)
    }
    
  }
}
