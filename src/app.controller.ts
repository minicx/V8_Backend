import { Controller, Request, Post, Body } from '@nestjs/common';

import { AuthService } from './auth/auth.service';

@Controller('auth')
export class AppController {
  constructor(private authService: AuthService) {}
  @Post('login')
  async login(@Body() user) {
    return this.authService.login(user);
  }
}
