import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { BaseError, ValidationError } from 'sequelize';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { CreateLinkerDto } from './dto/create-linker.dto';
import { LinkerService } from './linker.service';

@Controller('/api/linkers')
export class LinkerController {
  constructor(private LinkerService: LinkerService) {}

  @UseGuards(JwtAuthGuard)
  @Post('/')
  async createLinker(@Body() dto: CreateLinkerDto) {
    console.log(dto);
    try {
      return await this.LinkerService.createLinker(dto);
    } catch (err) {
      if (err instanceof BaseError) {
        if (err instanceof ValidationError) {
          throw new HttpException(
            {
              reason: 'This not a correct data',
              error: err.errors[0],
            },
            HttpStatus.BAD_REQUEST,
          );
        } else {
          throw new HttpException(
            { reason: 'Internal error', error: err.message },
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
      } else {
        throw new HttpException(
          'Unknown error',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
  @Get('/')
  async getLinkers() {
    return await this.LinkerService.getLinkers();
  }
}
