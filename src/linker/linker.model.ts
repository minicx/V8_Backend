import { Column, Model, Table } from 'sequelize-typescript';
import { DataType } from 'sequelize-typescript';
import { CreateLinkerDto } from './dto/create-linker.dto';
@Table({
  tableName: 'linkers',
  timestamps: true,
})
export class Linker
  extends Model<Linker, CreateLinkerDto>
  implements CreateLinkerDto
{
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
    allowNull: false,
    validate: {
      notNull: true,
      notEmpty: true,
    },
  })
  title: string;

  @Column({
    type: DataType.STRING,
  })
  parent?: string;

  declare createdAt: Date;
  declare updatedAt: Date;
}
