export class CreateLinkerDto {
  readonly title: string;
  readonly parent?: string;
}
