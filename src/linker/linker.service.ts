import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { isString } from 'lodash';
import { Sequelize } from 'sequelize-typescript';
import { CreateLinkerDto } from './dto/create-linker.dto';
import { Linker } from './linker.model';

@Injectable()
export class LinkerService {
  constructor(@InjectModel(Linker) private linker: typeof Linker) {}
  async getLinkers() {
    return await this.linker.findAll({
        order: [
            ['createdAt', 'DESC']
        ]
    });
  }
  async createLinker(dto: CreateLinkerDto) {
    if (isString(dto.parent)) {
      if (
        (await this.getLinkers()).some((linker) => {
          if (linker.title === dto.parent) {
            return true;
          }
        })
      ) {
        return await this.linker.create(dto);
      }
    }
    return await this.linker.create({
      title: dto.title,
    });
  }
}
