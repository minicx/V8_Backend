import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Linker } from './linker.model';
import { LinkerController } from './linker.controller';
import { LinkerService } from './linker.service';
import { SequelizeModule } from '@nestjs/sequelize';
@Module({
  imports: [SequelizeModule.forFeature([Linker])],
  controllers: [LinkerController],
  providers: [LinkerService],
})
export class LinkerModule {}
