import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Form } from './form.model';
import { FormController } from './form.controller';
import { FormService } from './form.service';
import { SequelizeModule } from '@nestjs/sequelize';
@Module({
  imports: [SequelizeModule.forFeature([Form])],
  controllers: [FormController],
  providers: [FormService],
})
export class FormModule {}
