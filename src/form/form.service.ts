import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { CreateFormDto } from './dto/create-form.dto';
import { Form } from './form.model';

@Injectable()
export class FormService {
  constructor(@InjectModel(Form) private forms: typeof Form) {}
  async getForms() {
    return await this.forms.findAll();
  }
  async createForm(dto: CreateFormDto) {
    return await this.forms.create(dto);
  }
}
