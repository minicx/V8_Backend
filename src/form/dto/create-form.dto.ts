export class CreateFormDto {
  readonly title: string;
  readonly description: string;
}
