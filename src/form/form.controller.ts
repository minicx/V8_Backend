import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { BaseError, ValidationError } from 'sequelize';
import { CreateFormDto } from './dto/create-form.dto';
import { FormService } from './form.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('/api/forms')
export class FormController {
  constructor(private FormService: FormService) {}

  @UseGuards(JwtAuthGuard)
  @Post('/')
  async createForm(@Body() dto: CreateFormDto) {
    console.log(dto);
    try {
      return await this.FormService.createForm(dto);
    } catch (err) {
      if (err instanceof BaseError) {
        if (err instanceof ValidationError) {
          throw new HttpException(
            {
              reason: 'This not a correct data',
              error: err.errors[0],
            },
            HttpStatus.BAD_REQUEST,
          );
        } else {
          throw new HttpException(
            { reason: 'Internal error', error: err.message },
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
      } else {
        throw new HttpException(
          'Unknown error',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    }
  }
  @Get('/')
  async getForms() {
    return await this.FormService.getForms();
  }
}
