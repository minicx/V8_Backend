import { Column, Model, Table } from 'sequelize-typescript';
import { DataType } from 'sequelize-typescript';
import { CreateFormDto } from './dto/create-form.dto';
@Table({
  tableName: 'forms',
  timestamps: true,
})
export class Form extends Model<Form, CreateFormDto> implements CreateFormDto {
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
    allowNull: false,
    validate: {
      notNull: true,
    },
  })
  title: string;

  @Column({ type: DataType.STRING, defaultValue: '' })
  description: string;

  declare createdAt: Date;
  declare updatedAt: Date;
}
