import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Form } from './form/form.model';
import { SequelizeModule } from '@nestjs/sequelize';
import { FormModule } from './form/form.module';
import { Linker } from './linker/linker.model';
import { LinkerModule } from './linker/linker.module';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.${process.env.ENV}.env`,
    }),
    SequelizeModule.forRoot({
      dialect: 'sqlite',
      storage: 'db.sqlite',
      models: [Form, Linker],
      autoLoadModels: true,
      sync: { force: process.env.ENV === 'development' ?? true },
    }),
    FormModule,
    LinkerModule,
    AuthModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
